package config

import (
	"testing"
	"fmt"
	"os"
	"io/ioutil"
)

func TestInitConfig_FileNotExists(t *testing.T) {
	err := Init()
	if err != nil {
		t.Fatalf("expected nil, got %s", err)
	}

	assertFileExists(t, filePath())
	assertFileContains(t, filePath(), defaultConfigContent)
}

func TestInitConfig_FileExists(t *testing.T) {
	writeFile(t, filePath(), alternateConfigContent)

	err := Init()
	if err != nil {
		t.Fatalf("expected nil, got %s", err)
	}

	assertEquals(t, "http://localhost:6060", Default.Repo.URL)
}

func TestGetConfig_WithPrefix(t *testing.T) {
	var prefix = "test"
	writeFile(t, filePathp(prefix), alternateConfigContent)
	defer removeConfigp(prefix)

	c, err := Get(prefix)
	if err != nil {
		t.Fatalf("expected nil, got %s", err)
	}

	assertEquals(t, "http://localhost:6060", c.Repo.URL)
}

func TestConfig_Save(t *testing.T) {
	var initURL = "http://localhost:8080"
	var newURL = "http://localhost:9090"
	var prefix = "test"
	writeFile(t, filePathp(prefix), defaultConfigContent)
	defer removeConfigp(prefix)

	preResult, err := Get(prefix)
	if err != nil {
		t.Error(err)
	}

	assertEquals(t, initURL, preResult.Repo.URL)

	preResult.Repo.URL = newURL
	preResult.Save()

	result, err := Get(prefix)
	if err != nil {
		t.Error(err)
	}

	assertEquals(t, newURL, result.Repo.URL)
}

func TestMain(m *testing.M) {
	DefaultConfigDir = "."
	removeConfig()
	code := m.Run()
	removeConfig()
	os.Exit(code)
}

func filePath() string {
	return fmt.Sprintf("%s/%s", DefaultConfigDir, DefaultConfigFileName)
}

func filePathp(prefix string) string {
	return fmt.Sprintf("%s/%s-%s", DefaultConfigDir, prefix, DefaultConfigFileName)
}

func assertFileExists(t *testing.T, filePath string) {
	f, err := os.Open(filePath)
	if err != nil {
		t.Fatal("expected file, but file not found")
	}
	defer f.Close()
}

func assertFileContains(t *testing.T, filePath string, expected []byte) {
	f, err := os.Open(filePath)
	if err != nil {
		t.Fatal("expected content, but could not open")
	}

	result, err := ioutil.ReadAll(f)
	if err != nil {
		t.Fatal("expected content, but could not read")
	}

	if string(result) != string(expected) {
		t.Fatalf("expected [%s], but got [%s]", string(expected), string(result))
	}

}

func assertEquals(t *testing.T, expected, result string) {
	if expected != result {
		t.Fatalf("expected [%s], got [%s]", expected, result)
	}
}

func removeConfig() {
	os.Remove(filePath())
}

func removeConfigp(prefix string) {
	os.Remove(filePathp(prefix))
}

func writeFile(t *testing.T, filePath string, content []byte) {
	f, err := os.Create(filePath)
	if err != nil {
		t.Error(err)
	}
	defer f.Close()

	_, err = f.Write(content)
	if err != nil {
		t.Error(err)
	}
}

var defaultConfigContent = []byte(`repo:
  url: http://localhost:8080
  username: ""
  password: ""
`)

var alternateConfigContent = []byte(`repo:
  url: http://localhost:6060
  username: ""
  password: ""
`)