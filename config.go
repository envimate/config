package config

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"os"
	"io/ioutil"
)

var (
	DefaultConfigDir      = os.Getenv("HOME")+"/.envimate"
)

const (
	DefaultConfigFileName = "config.yml"
)

var Default *Config

//Get reads a config file and returns it contents.
func Get(prefix string) (*Config, error) {
	var dirName = DefaultConfigDir
	var fileName string
	var filePath string

	if prefix == "" {
		fileName = DefaultConfigFileName
	} else {
		fileName = fmt.Sprintf("%s-%s", prefix, DefaultConfigFileName)
	}

	filePath = fmt.Sprintf("%s/%s", dirName, fileName)
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var c *Config
	err = yaml.Unmarshal(b, &c)

	c.usedPrefix = prefix

	return c, err
}

//Init populates the default config from the default config file.
func Init() error {
	os.MkdirAll(DefaultConfigDir, 0777)

	c, err := Get("")
	if err == nil {
		Default = c
		return nil
	}

	c, err = Create("")
	if err != nil {
		return err
	}

	Default = c
	return nil
}

//Create returns a default configuration and writes it to the filesystem.
func Create(prefix string) (*Config, error) {
	c := &Config{
		prefix,
		Repo{
			URL: "http://localhost:8080",
			Username: "",
			Password: "",
		},
	}

	err := c.Save()
	if err != nil {
		return nil, err
	}
	return c, nil
}

//Config reflects the content of the configuration file.
type Config struct {
	usedPrefix string
	Repo Repo
}

//Save overwrites the configuration file with the content of this Config.
func (c *Config) Save() error{
	var dirName = DefaultConfigDir
	var fileName string
	var filePath string

	if c.usedPrefix == "" {
		fileName = DefaultConfigFileName
	} else {
		fileName = fmt.Sprintf("%s-%s", c.usedPrefix, DefaultConfigFileName)
	}
	filePath = fmt.Sprintf("%s/%s", dirName, fileName)

	b, err := yaml.Marshal(c)
	if err != nil {
		return err
	}

	_ = os.Remove(filePath)
	f, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write(b)
	return err
}

//Repo reflects the Envibin repository configuration.
type Repo struct {
	URL string
	Username string
	Password string
}
