# Envibin Config

This packages helps handling configuration in a universal way. Envibin Config allows for reading and writing configuration located in `$HOME/.envimate/`. It supports a default config and prefixed configs.

### Default
To read the default configuration or create a new one simply call `InitConfig()`. If no configuration exists, the file will be created.
```
package main

import "bitbucket.org/envimate/config"

func main() {
    err := config.Init()
    if err != nil {
        panic(err)
    }

    // Read default config
    fmt.Println(config.Default.Repo.URL)
}
```

### Prefixed
To read a prefixed configuration file call the `Get()` function. When no file is found, an error will be returned.
```
package main

import "bitbucket.org/envimate/config"

func main() {
    cfg, err := config.Get("someCompany")
    if err != nil {
        panic(err)
    }

    // Read default config
    fmt.Println(config.Default.Repo.URL)
}
```
This will try and read the file `$HOME/.envimate/someCompany-config.yaml`, but fail if it isn't there.

### Create
To create/overwrite a configuration one can call the `Create(prefix string)` function. The prefix is used as mentioned above. Give an empty prefix will create/overwrite a new default configuration.

### Changing configuration
Configuration can be adjusted and saved;
```
package main

import "bitbucket.org/envimate/config"

func main() {
    err := config.Init()
    if err != nil {
        panic(err)
    }

    // Change default config
    config.Default.Repo.URL = "http://localhost:9000"
    config.Default.Save()
}
```
This will persist configuration to the right file and also works on prefixed configurations.